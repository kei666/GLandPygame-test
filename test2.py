import pygame
import numpy
from pygame.locals import *
from OpenGL.GL import *
from OpenGL.GLU import *
import numpy as np
from PIL import Image
import cv2
from PIL import ImageOps
import math
import base64
import os
#from OpenGL.GLUT import *

class Triforce(object):
    def __init__(self):
        self.pose = self.set_pose()
        self.phi = 0
        self.theta = 0
        self.tex = 1
        self.light = [0, 0, -5]

    def set_pose(self):
        return np.array([[1, 0, 0, 0],
                         [0, 1, 0, 0],
                         [0, 0, 1, -1.5]])

    def load_background(self, ppm_path):
        '''
        :param ppm_path:
        :return:
        '''
        ppm = Image.open(ppm_path)
        w, h = ppm.size  # 幅・高さのタプル
        data = ppm.tobytes()  # 画像データを文字列化
        self.tex2 = glGenTextures(1)
        # 取得した番号のテクスチャを使うように設定
        glBindTexture(GL_TEXTURE_2D, self.tex2)
        #glPixelStorei(GL_UNPACK_ALIGNMENT, 1)
        if (ppm.mode == "RGBA"):
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h,
                         0, GL_RGBA, GL_UNSIGNED_BYTE, data)
        else:
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h,
                         0, GL_RGB, GL_UNSIGNED_BYTE, data)

    def ppm2texture(self, ppm_path):
        u"""
        PPM ファイルを読み込んでテクスチャを返す
        """
        ppm = Image.open(ppm_path)
        self.w, self.h = ppm.size # 幅・高さのタプル
        data = ppm.tobytes() # 画像データを文字列化
        self.tex = glGenTextures(1)
        # 取得した番号のテクスチャを使うように設定
        glBindTexture(GL_TEXTURE_2D, self.tex)
        #glPixelStorei(GL_UNPACK_ALIGNMENT,1)
        if(ppm.mode == "RGBA"):
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, self.w, self.h,
                    0, GL_RGBA, GL_UNSIGNED_BYTE, data)
        else:
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, self.w, self.h,
                         0, GL_RGB, GL_UNSIGNED_BYTE, data)
        #GL_UNSIGNED_BYTE

    def draw(self):
        """立方体を描く"""
    #    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE)
        # 1 つ目のテクスチャを設定
        glBindTexture(GL_TEXTURE_2D, self.tex)
        # テクスチャマップの方法を設定
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
        #glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)

        glBegin(GL_QUADS) # 四角形を描く
        glTexCoord2f(0.0, 1) # テクスチャ画像での位置を指定
        glVertex3f(-1, 1, 0) # 頂点をセット

        glTexCoord2f(0, 0)
        glVertex3f(-1, -1, 0.0)

        glTexCoord2f(1, 0)
        glVertex3f(1, -1, 0.0)

        glTexCoord2f(1, 1)
        glVertex3f(1, 1, 0)
        glEnd()

        glBegin(GL_QUADS)

        glEnd()

    def draw_cylinder(self, radius, height, sides):
        pi = 3.1415

        glBindTexture(GL_TEXTURE_2D, self.tex)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)

        glBegin(GL_QUAD_STRIP)

        for i in range(sides + 1):
            t = i * 2 * pi / sides
            glNormal3f(math.cos(t), 0.0, math.sin(t))
            if (t > pi):
                glTexCoord2f((math.cos(t) + 1) / 2, 0)
                glVertex3f(radius * math.cos(t), -height / 2, radius * math.sin(t))
                glTexCoord2f((math.cos(t) + 1) / 2, 1)
                glVertex3f(radius * math.cos(t), height / 2, radius * math.sin(t))

        glEnd()

    def update_light_pose(self, z, x):
        '''
        :param pose:　３次元位置[x, y, z]
        :param z: Z軸回転 (0 < z < 360)
        :param x: X軸回転 (0 < x < 90)
        :return: light_pose[x, y, z, 1.0]
        '''
        pz = math.pi / 180 * z
        px = math.pi / 180 * x

        rz = np.matrix([[math.cos(pz), -math.sin(pz), 0],
                        [math.sin(pz), math.cos(pz), 0],
                        [0, 0, 1]])

        rx = np.matrix([[1, 0, 0],
                        [0, math.cos(px), -math.sin(px)],
                        [0, math.sin(px), math.cos(px)]])

        position = np.array((np.linalg.inv(rz) * np.linalg.inv(rx) * np.matrix(self.light).T).T)[0]

        return [position[0], position[1], -position[2], 1.0]


    def update_camera_pose(self, rz, rx, rl, x, y, w):
        '''

        :param rz: Z軸回転 (0 < rz < 360)
        :param rx: X軸回転 (0 < rx < 90)
        :param rl: 光軸回転 (0 < rl < 360)
        :param x: 平行移動
        :param y: 平行移動
        :param w: ズーム　何倍
        '''

        #z軸回転
        self.pose = self.set_pose()
        pz = math.pi / 180 * rz
        px = math.pi / 180 * rx
        pl = math.pi / 180 * rl
        rrz = np.array([[math.cos(pz), -math.sin(pz), 0],
                     [math.sin(pz), math.cos(pz), 0],
                     [0, 0, 1]])

        rrx = np.array([[1, 0, 0],
                        [0, math.cos(px), -math.sin(px)],
                        [0, math.sin(px), math.cos(px)]])

        a = np.linalg.inv(np.matrix(rrz)) * np.linalg.inv(np.matrix(rrx))
        self.pose = np.matrix(a) * np.matrix(self.pose)

        #光軸回転
        l =  pz *  self.pose[:, 2:3]
        lr, jacv = cv2.Rodrigues(l)
        self.pose = np.matrix(lr) * np.matrix(self.pose)

        l = pl * self.pose[:, 2:3]
        lr, jacv = cv2.Rodrigues(l)
        self.pose = np.matrix(lr) * np.matrix(self.pose)

        self.pose[:, 3:4] /= w

        self.pose[0, 3] += x
        self.pose[1, 3] += y


    def draw_background(self):

        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        glOrtho(0, 1, 0, 1, -1, 1)
        glDisable(GL_DEPTH_TEST)
        glDisable(GL_LIGHTING)
        glDepthMask(GL_FALSE)
        glMatrixMode(GL_MODELVIEW)
        glLoadIdentity()
        glBindTexture(GL_TEXTURE_2D, self.tex2)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
        glBegin(GL_QUADS)
        glTexCoord2f(0, 1)
        glVertex2f(0, 0)

        glTexCoord2f(1, 1)
        glVertex2f(1, 0)

        glTexCoord2f(1, 0)
        glVertex2f(1, 1)

        glTexCoord2f(0, 0)
        glVertex2f(0, 1)

        glEnd()

        glDepthMask(GL_TRUE)
        glEnable(GL_DEPTH_TEST)
        glEnable(GL_CULL_FACE)
        glCullFace(GL_BACK)  # 裏面をカリング
        glEnable(GL_LIGHTING)
        glMatrixMode(GL_PROJECTION)
        glLoadIdentity();
        gluPerspective(73.0, float(960) / float(1280), 0.0001, 100000.0)
        glMatrixMode(GL_MODELVIEW)
        glLoadIdentity()

    def init(self, window_size):
        w, h = window_size
        glClearColor(0.0, 0.0, 0.0, 0.0)
        glEnable(GL_DEPTH_TEST)
        glEnable(GL_TEXTURE_2D)
        glEnable(GL_LIGHTING)  # 照明を有効化
        glEnable(GL_LIGHT0)  # 0番目の照明を有効化
        glViewport(0, 0, w, h)
        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        gluPerspective(73.0, float(w) / float(h), 0.0001, 100000.0)
        glMatrixMode(GL_MODELVIEW)

    def draw2(self):
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)


        glMatrixMode(GL_MODELVIEW)
        glLoadIdentity()

        light_ambient = [1.0, 1.0, 1.0, 1.0]  # 環境光（白色）
        light_diffuse = [1.0, 1.0, 1.0, 1.0]  # 拡散光（白色）
        light_specular = [1.0, 1.0, 1.0, 1.0]  # 鏡面光（白色）
        light_position = self.update_light_pose(90, 0)

        # 照明
        glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient)
        glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse)
        glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular)
        glLightfv(GL_LIGHT0, GL_POSITION, light_position)

        glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient)

        glLightfv(GL_LIGHT0, GL_POSITION, light_position)

        # self.update_camera_pose(0, 0, math.pi/6)
        self.update_camera_pose(0, 0, 0, 0, 0, 0.7)

        self.draw_background()
        # 視野変換：カメラの位置と方向のセット
        gluLookAt(self.pose[0, 3], self.pose[1, 3], self.pose[2, 3],
                  self.pose[0, 3] + self.pose[0, 2], self.pose[1, 3] + self.pose[1, 2], self.pose[2, 3] + self.pose[2, 2],
                  -self.pose[0, 1], -self.pose[1, 1], -self.pose[2, 1])

        # self.draw_background()
        self.draw_cylinder(radius=0.5, height=self.h/self.w, sides=50)  #  画像を描く

        glFlush()

    def exit_check(self, event, inkey=None):
        u"""
        終了チェック
        """
        if event.type == QUIT:
            return True
        if inkey is not None:
            if event.type == KEYDOWN and event.key == inkey:
                return True
        return False

    def test(self):
        print("1")
        pygame.init()
        print("2")
        window_size = (960, 1280)
        screen = pygame.display.set_mode(
                window_size,
                OPENGL | DOUBLEBUF,
                )
        self.init(window_size)
        print("3")
        clock = pygame.time.Clock()

        lphi = [i * 10 for i in range(18)]
        ltheta = [i * 5 for i in range(-6, 7)]

        while True:#for i in range(3480):
            #clock.tick(120)
            #dir_name = "train2/" + str(i)
            #os.mkdir(dir_name)
            img_name = "0.png"#"square/" + str(i) + ".png"
            self.ppm2texture(img_name)
            self.load_background("1.png")
            index = 0
            for p in lphi:
                for t in ltheta:
                    # self.phi = math.pi / 180 * p
                    # self.theta = math.pi / 180 * t
                    self.draw2()
                    pygame.display.flip()
                    # if index != 0:
                    #     save_name = dir_name + "/" + str(index - 1) + ".jpg"
                    #     pygame.image.save(screen, save_name)
                    # index += 1

                    for event in pygame.event.get():
                        if self.exit_check(event, K_ESCAPE):
                            string_image = pygame.image.tostring(screen, 'RGB')
                            temp_surf = pygame.image.fromstring(string_image, (window_size[0], window_size[1]), 'RGB')
                            img = pygame.surfarray.array3d(temp_surf)
                            img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
                            img = img.transpose(1, 0, 2)
                            cv2.imwrite("output.png", img)
                            return

        # while True:
        #     for p in lphi:
        #         for t in ltheta:
        #             self.phi = math.pi / 180 * p
        #             self.theta = math.pi / 180 * t
        #             self.tex = self.ppm2texture("2560.jpg")
        #             self.draw2()
        #             pygame.display.flip()
        #             for event in pygame.event.get():
        #                 if self.exit_check(event, K_ESCAPE):
        #                     return
        pygame.quit()

if __name__ == '__main__':
    print("object_load start")
    o = Triforce()
    print("object_load end")
    o.test()
